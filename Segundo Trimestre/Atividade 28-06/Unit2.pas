unit Unit2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, System.Net.URLClient,
  System.Net.HttpClient, System.Net.HttpClientComponent, Vcl.StdCtrls, Unit3;

type
  TForm2 = class(TForm)
    NetHTTPClient1: TNetHTTPClient;
    edNome: TEdit;
    edSenha: TEdit;
    btLogin: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    procedure btLoginClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm2.btLoginClick(Sender: TObject);
var
  Content, nome, senha : String;

begin
  nome := edNome.Text;
  senha := edSenha.Text;
  Content := NetHTTPClient1.Get('https://venson.net.br/ws?user='+nome+'&pass='+senha).ContentAsString;

  if Content = 'true' then
    begin
      Form3.ShowModal();

    end
  else
    begin
      showmessage ('Usuario ou senha incorretos');

    end;

end;

end.
