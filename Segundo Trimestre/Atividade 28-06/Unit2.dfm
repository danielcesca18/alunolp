object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 129
  ClientWidth = 169
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 135
    Width = 155
    Height = 13
    Caption = 'Quando a vida te der limoes n'#227'o'
  end
  object Label2: TLabel
    Left = 8
    Top = 154
    Width = 151
    Height = 13
    Caption = 'fa'#231'a uma limonada. Fa'#231'a a vida'
  end
  object Label3: TLabel
    Left = 8
    Top = 173
    Width = 149
    Height = 13
    Caption = 'pegar os lim'#245'es de volta! Fique'
  end
  object Label4: TLabel
    Left = 8
    Top = 192
    Width = 132
    Height = 13
    Caption = 'furioso!  Eu n'#227'o quero seus'
  end
  object Label5: TLabel
    Left = 8
    Top = 211
    Width = 152
    Height = 13
    Caption = 'malditos lim'#245'es! O que '#233' que eu'
  end
  object Label6: TLabel
    Left = 8
    Top = 230
    Width = 149
    Height = 13
    Caption = 'vou fazer com eles? Exija ver o'
  end
  object Label7: TLabel
    Left = 8
    Top = 249
    Width = 135
    Height = 13
    Caption = ' gerente da vida! Voc'#234' sabe'
  end
  object Label8: TLabel
    Left = 8
    Top = 268
    Width = 150
    Height = 13
    Caption = ' quem eu sou? Eu sou o homem'
  end
  object Label9: TLabel
    Left = 8
    Top = 287
    Width = 142
    Height = 13
    Caption = ' que vai incendiar a sua casa!'
  end
  object Label10: TLabel
    Left = 8
    Top = 307
    Width = 135
    Height = 13
    Caption = 'Com os lim'#245'es! Eu vou fazer'
  end
  object Label11: TLabel
    Left = 8
    Top = 326
    Width = 147
    Height = 13
    Caption = ' meus engenheiros inventarem'
  end
  object Label12: TLabel
    Left = 8
    Top = 345
    Width = 116
    Height = 13
    Caption = ' um lim'#227'o inflam'#225'vel que'
  end
  object Label13: TLabel
    Left = 8
    Top = 364
    Width = 131
    Height = 13
    Caption = ' incendeie a sua casa toda!'
  end
  object Label14: TLabel
    Left = 86
    Top = 383
    Width = 75
    Height = 13
    Caption = '- Cave Johnson'
  end
  object edNome: TEdit
    Left = 24
    Top = 8
    Width = 121
    Height = 21
    TabOrder = 0
    TextHint = 'Nome'
  end
  object edSenha: TEdit
    Left = 24
    Top = 56
    Width = 121
    Height = 21
    TabOrder = 1
    TextHint = 'Senha'
  end
  object btLogin: TButton
    Left = 24
    Top = 96
    Width = 121
    Height = 25
    Caption = 'Login'
    TabOrder = 2
    OnClick = btLoginClick
  end
  object NetHTTPClient1: TNetHTTPClient
    Asynchronous = False
    ConnectionTimeout = 60000
    ResponseTimeout = 60000
    AllowCookies = True
    HandleRedirects = True
    UserAgent = 'Embarcadero URI Client/1.0'
    Left = 160
  end
end
