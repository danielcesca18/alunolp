unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Memo2: TMemo;
    procedure Button3Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  Memo1.MaxLength := (random(450));
  showMessage(inttostr(Memo1.MaxLength));
end;

procedure TForm1.Button2Click(Sender: TObject);
const
  CHARS = ['s', 'S'];

var
  i : integer;
  str : string;

begin
  str := trim(Memo1.Text);

  for i := 1 to length(str) do
    begin
      if (str[i] in CHARS) then
        begin
          showMessage('TEM "S"');
          break;
        end;
    end;

end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  Memo2.Text := trim(Memo1.Text);
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  Memo2.Text := lowerCase(Memo1.Text);
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
  Memo2.Text := upperCase(Memo1.Text);
end;

procedure TForm1.Button6Click(Sender: TObject);
begin
  Memo2.Text := stringReplace(Memo1.Text, 's', 't', [rfReplaceAll, rfIgnoreCase]);
end;

end.
