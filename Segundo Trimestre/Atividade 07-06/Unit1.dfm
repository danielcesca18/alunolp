object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 216
  ClientWidth = 430
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 8
    Top = 8
    Width = 201
    Height = 105
    Lines.Strings = (
      'Memo1')
    TabOrder = 0
  end
  object Button1: TButton
    Left = 23
    Top = 128
    Width = 75
    Height = 25
    Caption = 'MaxLength'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 176
    Top = 128
    Width = 75
    Height = 25
    Caption = 'Have S or not'
    TabOrder = 2
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 321
    Top = 128
    Width = 75
    Height = 25
    Caption = 'Trim'
    TabOrder = 3
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 23
    Top = 175
    Width = 75
    Height = 25
    Caption = 'LowerCase'
    TabOrder = 4
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 176
    Top = 175
    Width = 75
    Height = 25
    Caption = 'UpperCase'
    TabOrder = 5
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 321
    Top = 175
    Width = 75
    Height = 25
    Caption = 'S to T'
    TabOrder = 6
    OnClick = Button6Click
  end
  object Memo2: TMemo
    Left = 221
    Top = 8
    Width = 201
    Height = 105
    Lines.Strings = (
      'Memo1')
    ReadOnly = True
    TabOrder = 7
  end
end
