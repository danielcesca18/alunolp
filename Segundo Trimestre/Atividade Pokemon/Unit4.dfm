object DataModule4: TDataModule4
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 488
  Width = 712
  object FDPhysMySQLDriverLink1: TFDPhysMySQLDriverLink
    VendorLib = 
      'C:\Users\informatica 03\Documents\EasyPHP-DevServer-14.1VC9\bina' +
      'ries\mysql\lib\libmysql.dll'
    Left = 48
    Top = 24
  end
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=bd_pokemon'
      'User_Name=root'
      'DriverID=MySQL')
    Connected = True
    Left = 168
    Top = 24
  end
  object FDQuery1: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select * from pokemon')
    Left = 264
    Top = 24
  end
  object FDQuery2: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select * from treinador')
    Left = 264
    Top = 88
  end
  object DataSource1: TDataSource
    DataSet = FDQuery1
    Left = 336
    Top = 24
  end
  object DataSource2: TDataSource
    DataSet = FDQuery2
    Left = 336
    Top = 88
  end
end
