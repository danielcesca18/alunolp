object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 210
  ClientWidth = 366
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Editar: TButton
    Left = 240
    Top = 103
    Width = 75
    Height = 25
    Caption = 'Editar'
    TabOrder = 0
    OnClick = EditarClick
  end
  object Inserir: TButton
    Left = 240
    Top = 134
    Width = 75
    Height = 25
    Caption = 'Inserir'
    TabOrder = 1
    OnClick = InserirClick
  end
  object Deletar: TButton
    Left = 240
    Top = 165
    Width = 75
    Height = 25
    Caption = 'Deletar'
    TabOrder = 2
    OnClick = DeletarClick
  end
  object Panel1: TPanel
    Left = 199
    Top = 8
    Width = 159
    Height = 89
    TabOrder = 3
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 10
      Height = 13
      Caption = 'Id'
    end
    object Label2: TLabel
      Left = 16
      Top = 27
      Width = 46
      Height = 13
      Caption = 'Treinador'
    end
    object Label3: TLabel
      Left = 16
      Top = 46
      Width = 27
      Height = 13
      Caption = 'Nome'
    end
    object Label4: TLabel
      Left = 16
      Top = 65
      Width = 23
      Height = 13
      Caption = 'N'#237'vel'
    end
    object DBText1: TDBText
      Left = 86
      Top = 8
      Width = 65
      Height = 17
      DataField = 'Id'
      DataSource = DataModule4.DataSource1
    end
    object DBText2: TDBText
      Left = 86
      Top = 65
      Width = 65
      Height = 17
      DataField = 'N'#237'vel'
      DataSource = DataModule4.DataSource1
    end
    object DBText3: TDBText
      Left = 86
      Top = 46
      Width = 65
      Height = 17
      DataField = 'Nome'
      DataSource = DataModule4.DataSource1
    end
    object DBText4: TDBText
      Left = 86
      Top = 27
      Width = 65
      Height = 17
      DataField = 'Nome'
      DataSource = DataModule4.DataSource2
    end
  end
  object DBLookupListBox1: TDBLookupListBox
    Left = 24
    Top = 8
    Width = 145
    Height = 173
    KeyField = 'Id'
    ListField = 'Nome'
    ListSource = DataModule4.DataSource1
    TabOrder = 4
  end
end
