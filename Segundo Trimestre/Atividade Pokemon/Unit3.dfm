object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Form3'
  ClientHeight = 158
  ClientWidth = 234
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 31
    Top = 24
    Width = 27
    Height = 13
    Caption = 'Nome'
  end
  object Label2: TLabel
    Left = 12
    Top = 51
    Width = 46
    Height = 13
    Caption = 'Treinador'
  end
  object Label3: TLabel
    Left = 31
    Top = 78
    Width = 23
    Height = 13
    Caption = 'N'#237'vel'
  end
  object DBEdit1: TDBEdit
    Left = 64
    Top = 16
    Width = 145
    Height = 21
    DataField = 'Nome'
    DataSource = DataModule4.DataSource1
    TabOrder = 0
  end
  object DBEdit3: TDBEdit
    Left = 64
    Top = 70
    Width = 145
    Height = 21
    DataField = 'N'#237'vel'
    DataSource = DataModule4.DataSource1
    TabOrder = 1
  end
  object Button1: TButton
    Left = 44
    Top = 112
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 142
    Top = 112
    Width = 75
    Height = 25
    Caption = 'Salvar'
    TabOrder = 3
    OnClick = Button2Click
  end
  object DBLookupComboBox1: TDBLookupComboBox
    Left = 64
    Top = 43
    Width = 145
    Height = 21
    DataField = 'Id_treinador'
    DataSource = DataModule4.DataSource1
    KeyField = 'Id'
    ListField = 'Nome'
    ListSource = DataModule4.DataSource2
    TabOrder = 4
  end
end
