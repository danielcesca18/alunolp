unit Unit2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Unit4, Vcl.DBCtrls, Vcl.StdCtrls,
  Vcl.ExtCtrls;

type
  TForm2 = class(TForm)
    Editar: TButton;
    Inserir: TButton;
    Deletar: TButton;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    DBText3: TDBText;
    DBText4: TDBText;
    DBLookupListBox1: TDBLookupListBox;
    procedure EditarClick(Sender: TObject);
    procedure InserirClick(Sender: TObject);
    procedure DeletarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

uses Unit3;

procedure TForm2.DeletarClick(Sender: TObject);
begin
  DataModule4.FDQuery1.Delete;
end;

procedure TForm2.EditarClick(Sender: TObject);
begin
  Form3.showmodal;
end;

procedure TForm2.InserirClick(Sender: TObject);
begin
  DataModule4.FDQuery1.Append;
  Form3.showmodal;
end;

end.
