object Form1: TForm1
  Left = 790
  Top = 261
  Anchors = [akLeft, akTop, akRight, akBottom]
  Caption = 'Form1'
  ClientHeight = 240
  ClientWidth = 224
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 225
    Height = 241
    Color = clTeal
    TabOrder = 0
    object lb_main: TListBox
      Left = 8
      Top = 8
      Width = 121
      Height = 225
      ItemHeight = 13
      TabOrder = 0
      OnMouseUp = a
    end
    object bt_deletall: TButton
      Left = 144
      Top = 128
      Width = 75
      Height = 25
      Caption = 'Excluir tudo'
      TabOrder = 1
      OnClick = bt_deletallClick
    end
    object bt_deletselec: TButton
      Left = 144
      Top = 88
      Width = 75
      Height = 25
      Caption = 'Excluir Selec.'
      TabOrder = 2
      OnClick = bt_deletselecClick
    end
    object bt_add: TButton
      Left = 144
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Adicionar'
      TabOrder = 3
      OnClick = bt_addClick
    end
    object bt_edit: TButton
      Left = 144
      Top = 48
      Width = 75
      Height = 25
      Caption = 'Editar'
      TabOrder = 4
      OnClick = bt_editClick
    end
    object bt_save: TButton
      Left = 144
      Top = 168
      Width = 75
      Height = 25
      Caption = 'Salvar'
      TabOrder = 5
      OnClick = bt_saveClick
    end
    object bt_load: TButton
      Left = 144
      Top = 208
      Width = 75
      Height = 25
      Caption = 'Load'
      TabOrder = 6
      OnClick = bt_loadClick
    end
  end
end
