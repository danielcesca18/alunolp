unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    bt_add: TButton;
    lb_main: TListBox;
    bt_deletselec: TButton;
    bt_deletall: TButton;
    Panel1: TPanel;
    bt_edit: TButton;
    bt_save: TButton;
    bt_load: TButton;
    procedure bt_addClick(Sender: TObject);
    procedure bt_deletallClick(Sender: TObject);
    procedure bt_deletselecClick(Sender: TObject);
    procedure bt_editClick(Sender: TObject);
    procedure bt_saveClick(Sender: TObject);
    procedure bt_loadClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure a(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
      Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.a(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
var
  i : integer;
begin
  for i := 0 to lb_main.items.count -1 do
    begin
      if lb_main.selected[i] then
        begin
          bt_edit.enabled := true;
        end
      else
        begin
          bt_edit.enabled := false;
        end;
    end;
end;

procedure TForm1.bt_addClick(Sender: TObject);
var
  dado : String;
begin
  dado := InputBox('Adicione um texto', 'Texto a ser adicionado: ', '');
  if (Trim(dado) <> '') then
    begin
      lb_main.items.add(dado);
    end;
end;

procedure TForm1.bt_deletallClick(Sender: TObject);
begin
  lb_main.items.clear;
end;

procedure TForm1.bt_deletselecClick(Sender: TObject);
begin
  lb_main.deleteSelected;
end;

procedure TForm1.bt_editClick(Sender: TObject);
var
  i : integer;
  dado : string;
begin
  for i := -1 + lb_main.items.count downto 0 do
    begin
      if (lb_main.selected[i]) then
      begin
        dado := inputBox('Texto a ser modificado: ' + lb_main.items[i], 'Novo texto: ', '');
        if (trim(dado) <> '') then
        begin
          lb_main.items[i] := dado;
        end;
      end;
    end;
end;

procedure TForm1.bt_loadClick(Sender: TObject);
begin
  lb_main.items.loadFromFile('LB_items');
end;

procedure TForm1.bt_saveClick(Sender: TObject);
begin
  lb_main.items.saveToFile('LB_items');
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  bt_edit.enabled := false;
end;

end.
